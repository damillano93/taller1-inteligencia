%% ejecutar perceptron OR 4 entradas
clc;
clear;
X=[-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1; 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1;0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1; 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1; 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 ];

Sd=[ -1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
d=2;
repeticiones = 1000;
in = zeros(1,repeticiones);
ti = zeros(1,repeticiones);
for j=1:1:repeticiones
[wi,w,count, tiempo]= per(X,Sd,d);
in(j) = count;
ti(j) = tiempo;
end
%% histograma repeticiones
figure;
hist(in)
title(['OR 4 entradas   iteraciones con: ' , num2str(repeticiones), ' repeticiones']);

%% histograma de tiempo de ejecucion
figure;
plot(ti)
prom=mean(ti)
title(['OR 4 entradas Tiempo de espera con ' , num2str(repeticiones), ' repeticiones'])
ylabel('Tiempo (s)');
xlabel('Repeticiones');
axis([0 repeticiones 0 0.0002])
