function [wi,w,count, tiempo] = per2(X,Sd,d,alpha)
tic
% Definicion de los pesos iniciales
L = length(X(1,:));
wn = length(X(:,1));
w=rands(1,wn);
wi=w;
Aw = zeros(1,wn);
count=0;
aux=0;
while aux < L

count=count+1;
    for i=1:1:L
        
    f=  sum(X(:,i).*w') ;  

        e = Sd(i)- f ;
        if e > d
 
         for k=1:1:wn
            Aw(k)=X(k,i)*alpha*e;
            w(k)=w(k)+Aw(k);
             
         end
         aux = 0;
        else
         aux=aux+1;

    end
end
end
tiempo = toc; 
end 