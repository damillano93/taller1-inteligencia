clear all;
close all;
clc;

% Definicion de los pesos iniciales
w0=rand;
w1=rand;
w2=rand;
% Se guardan los pesos iniciales
w0i=w0;
w1i=w1;
w2i=w2;

x0=1;
x1=[0 0 1 1];
x2=[0 1 0 1];
Sd=[-1 -1 -1 1];
count=0;
aux=0;


while aux < 4
aux=0;
count=count+1;
    for i=1:1:4
    f(i)=x0*w0+x1(i)*w1+x2(i)*w2;
        d=2;
        if f(i)<d
        Sr(i)=-1;
        else
        Sr(i)=1;
        end
    
        if Sr(i)==Sd(i)
        aux=aux+1;
        else
        Aw0=x0*Sd(i);
        Aw1=x1(i)*Sd(i);
        Aw2=x2(i)*Sd(i);
        
        w0=w0+Aw0;
        w1=w1+Aw1;
        w2=w2+Aw2;
    end
end
end

disp('----------------------------------------------------------')
disp('Los pesos iniciales son:') 
disp(['w0: ',num2str(w0i)]) 
disp(['w1: ',num2str(w1i)]) 
disp(['w2: ',num2str(w2i)])
disp('----------------------------------------------------------')
disp('Los pesos finales son:') 
disp(['w0: ',num2str(w0)]) 
disp(['w1: ',num2str(w1)]) 
disp(['w2: ',num2str(w2)]) 
disp('----------------------------------------------------------')
disp('----------------------------------------------------------')
disp(['El numero de Interacciones necesarias son:',num2str(count)]) 
disp('----------------------------------------------------------')

X1=0:0.01:3;
X2=(-w1/w2)*X1-(w0/w2)+(d/w2);
plot(X1,X2)
hold on
grid on


for j=1:1:4
if Sd(j)==1
    plot(x1(j),x2(j),'--gs')
else
    plot(x1(j),x2(j),'--rs')
end
end    