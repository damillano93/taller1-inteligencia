clc;
clear;
X=[1 1 1 1 1 1 1 1; 0 0 0 0 1 1 1 1; 0 0 1 1 0 0 1 1; 0 1 0 1 0 1 0 1];

Sd=[-1 -1 -1 -1 -1 -1 -1 1];
d=0.2;
alpha = 0.1;
for j=1:1:repeticiones
[wi,w,count, tiempo]= per2(X,Sd,d,alpha);
in(j) = count;
ti(j) = tiempo;
end
%% histograma repeticiones
figure;
hist(in)
title(['Numero de iteraciones con ' , num2str(repeticiones), ' repeticiones'])
%% histograma de tiempo de ejecucion
figure;
plot(ti)
prom=mean(ti)
title(['Tiempo de espera con ' , num2str(repeticiones), ' repeticiones'])
  