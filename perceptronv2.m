clear all
clc
        
disp('                 PREPCEPTRON SIMPLE                 ')
disp(' ')

%Compuertas de 2 entradas. Entradas X0, X1, X2

x0 = 1;
X1 = [0 1 0 1 0 1 0 1];
X2 = [0 0 1 1 0 0 1 1];
X3 = [0 0 0 0 1 1 1 1];
Yd = [0 1 2 3 4 5 6 7];
tolerancia=input('Digite el valor de la tolerancia permitida:')
%Valor inicial de los pesos para
%compuertas de dos entradas y el factor de entrenamiento alpha 

w = rands(1,4);
w0_i = w(1);
w1_i = w(2);
w2_i = w(3);
w3_i = w(4);

          
          %Inicialmente tomamos 3 valores de alpha        
          alpha = 0.1:0.1:0.9;
          c=length(alpha);
          
          for j=1:1:c
          w0 = w(1);
          w1 = w(2);
          w2 = w(3);
          w3 = w(4);
         
          %Yd=or(X1,X2); %Salida deseada
          aux=0;
          intentos(j,1)=0;

          while aux<8 && intentos(j,1)<100000
                      for i=1:1:8
                                  x1 = X1(i);
                                  x2 = X2(i);
                                  x3 = X3(i);
                                  neta=x0*w0+x1*w1+x2*w2+x3*w3;
                                  
                                  %Funcion de activacion paso
                                  %%%%%%%%%%%%%%%%%%%
                                  %if (neta<=0)
                                  %    y=0;
                                  %else 
                                  %    y=1;
                                  %end
                                  %%%%%%%%%%%%%%%%%%%
                                  % Funci�n de Activaci�n Lineal
                                  y=neta;
                                   
                                  e=Yd(i)-y
            
                                  if e>tolerancia
                                          w0=w0+alpha(1,j)*e*x0;
                                          w1=w1+alpha(1,j)*e*x1;
                                          w2=w2+alpha(1,j)*e*x2;
                                          w3=w3+alpha(1,j)*e*x3;
                                          aux=0;
                                  else 
                                       aux = aux+1;   
                                  end
            
                      end
                      intentos(j,1)=intentos(j,1)+1;
                      %aux=0;
          end
 
          wf(j,1)=w0; wf(j,2)=w1; wf(j,3)=w2; wf(j,4)=w3; 
          
          end
         

          disp(' ')
          disp('Compuerta OR de 2 entradas.')
          disp(' ')          
          disp(sprintf('Recuerde que los pesos iniciales fueron w0=%g, w1=%g y w2=%g',w0_i,w1_i,w2_i))  
          disp(' ')
          disp('La red neuronal queda entrenada con los siguientes pesos segun la siguiente tabla:')
          disp('_____________________________________________________________________')
          disp(' ')
          disp('alpha         w0        w1          w2          w3     #_intentos')
          disp(' ')
          disp('_____________________________________________________________________')
          for k=1:1:c
          disp(sprintf('%.1f \t %.6f \t %.6f \t %.6f \t %.6f \t \t %g',alpha(1,k),wf(k,1),wf(k,2),wf(k,3),wf(k,4),intentos(k,1)))
          %disp(sprintf('%.1f \t %.6f \t %.6f \t %.6f \t \t %g',alpha(1,2),wf(2,1),wf(2,2),wf(2,3),intentos(2,1)))
          %disp(sprintf('%.1f \t %.6f \t %.6f \t %.6f \t \t %g',alpha(1,3),wf(3,1),wf(3,2),wf(3,3),intentos(3,1)))          
          end
          disp('_____________________________________________________________________')