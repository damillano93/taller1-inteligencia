function [wi,w,count, tiempo] = per(X,Sd,d)
tic
% Definicion de los pesos iniciales
L = length(X(1,:));
wn = length(X(:,1));
w=rand(1,wn);
wi=w;
Aw = zeros(1,wn);
count=0;
aux=0;
while aux < L
aux=0;
count=count+1;
    for i=1:1:L
        
    f(i)=  sum(X(:,i).*w');   
        if f(i)<d
        Sr(i)=-1;
        else
        Sr(i)=1;
        end
    
        if Sr(i)==Sd(i)
        aux=aux+1;
        else
        for k=1:1:wn
            Aw(k)=X(k,i)*Sd(i);
            w(k)=w(k)+Aw(k);
        end

    end
end
end
tiempo = toc; 
end 