clc;
clear;
X=[1 1 1 1 1 1 1 1; 0 0 0 0 1 1 1 1; 0 0 1 1 0 0 1 1; 0 1 0 1 0 1 0 1];

Sd=[-1 -1 -1 -1 -1 -1 -1 1];
d=2;
[wi,w,count]= per(X,Sd,d);
disp('----------------------------------------------------------')
disp('Los pesos iniciales son:') 
disp(['wi: ',num2str(wi)]) 
disp('----------------------------------------------------------')
disp('Los pesos finales son:') 
disp(['w: ',num2str(w)]) 
disp('----------------------------------------------------------')
disp(['El numero de Interacciones necesarias son:',num2str(count)]) 
disp('----------------------------------------------------------')
X1=0:0.01:3;
X2=(-w(3)/w(4))*X1-(w(2)/w(4))-(w(1)/w(4))+(d/w(4));
plot(X1,X2)
hold on
grid on


for j=1:1:8
if Sd(j)==1
    plot(X(2,j),X(3,j),'--gs')
else
    plot(X(2,j),X(3,j),'--rs')
end
end    